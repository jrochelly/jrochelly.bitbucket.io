$(document).ready(function(){
    $(".c-btn").click(function(e) {
        $(".c-btn").removeClass("active");
        $(this).addClass("active");
        var noticia = $(this).attr("href");
        var offset = $('.carrousel ul').offset().left;
        if (offset != 0) {
            offset *= -1;
        }
        $('.carrousel').animate({
            scrollLeft: $("li" + noticia).offset().left + offset
        }, 500);
        return false;
    });

    $("a").click(function() {
        var url = $(this).attr("href");
        if (url.startsWith("#")){
            $("html, body").animate({
                scrollTop: $(url).offset().top
            }, 500);
            return false;
        }
    });

    $(".btn-mobile-menu").on("click", function(){
        $(this).parent().addClass("show-menu");
    });
    $(".btn-mobile-close-menu").on("click", function(){
        $(this).parent().parent().removeClass("show-menu");
    });

    $('select').formSelect();

});