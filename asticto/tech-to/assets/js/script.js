let end = new Date("10/23/2024 09:00 AM");

let _second = 1000;
let _minute = _second * 60;
let _hour = _minute * 60;
let _day = _hour * 24;
let timer;

function showRemaining() {
	let now = new Date();
	let distance = end - now;
	if (distance < 0) {
		clearInterval(timer);
		return;
	}
	let days = Math.floor(distance / _day);
	let hours = Math.floor((distance % _day) / _hour);
	let minutes = Math.floor((distance % _hour) / _minute);
	let seconds = Math.floor((distance % _minute) / _second);

	document.querySelector(".coming-counter .days").textContent = days;
	document.querySelector(".coming-counter .hours").textContent = hours;
	document.querySelector(".coming-counter .minutes").textContent = minutes;
	document.querySelector(".coming-counter .seconds").textContent = seconds;
}

timer = setInterval(showRemaining, 1000);
