// Toggle mobile menu
const mobileMenuBtn = document.querySelector(".mobile-menu-btn");
const mobileNav = document.querySelector(".mobile-nav");
const closeBtn = document.querySelector(".close-btn");

mobileMenuBtn.addEventListener("click", () => {
	mobileNav.classList.add("active");
	document.body.style.overflow = "hidden";
});

closeBtn.addEventListener("click", () => {
	mobileNav.classList.remove("active");
	document.body.style.overflow = "";
});

// Handle dropdowns
document.querySelectorAll(".dropdown-btn, .submenu-trigger").forEach((button) => {
	button.addEventListener("click", (e) => {
		e.stopPropagation();
		const isExpanded = button.getAttribute("aria-expanded") === "true";
		const menuId = button.getAttribute("aria-controls");
		const menu = document.getElementById(menuId);

		button.setAttribute("aria-expanded", !isExpanded);
		menu.classList.toggle("open");

		// Close other menus only if they're not parent menus
		document.querySelectorAll(".dropdown-content, .submenu-content, .mobile-submenu").forEach((otherMenu) => {
			if (otherMenu !== menu && !menu.contains(otherMenu) && !otherMenu.contains(menu)) {
				otherMenu.classList.remove("open");
				otherMenu.previousElementSibling?.setAttribute("aria-expanded", "false");
			}
		});
	});
});

// Close menus when clicking outside
document.addEventListener("click", (e) => {
	if (!e.target.closest(".dropdown, .mobile-dropdown")) {
		document.querySelectorAll(".dropdown-content, .submenu-content, .mobile-submenu").forEach((menu) => {
			menu.classList.remove("open");
			menu.previousElementSibling?.setAttribute("aria-expanded", "false");
		});
	}
});

// Handle keyboard navigation
document.addEventListener("keydown", (e) => {
	if (e.key === "Escape") {
		document.querySelectorAll(".dropdown-content, .submenu-content, .mobile-submenu").forEach((menu) => {
			menu.classList.remove("open");
			menu.previousElementSibling?.setAttribute("aria-expanded", "false");
		});
	}
});

// Handle window resize
window.addEventListener("resize", () => {
	if (window.innerWidth > 768) {
		mobileNav.classList.remove("active");
		document.body.style.overflow = "";
		document.querySelectorAll(".mobile-submenu").forEach((menu) => {
			menu.classList.remove("open");
			menu.previousElementSibling?.setAttribute("aria-expanded", "false");
		});
	}
});
