// window.addEventListener("load", () => {
const list = document.querySelector(".opportunities ul");
let listScrollWidth = list.scrollWidth - list.offsetWidth;
const SCROLL_STEP = 300;

window.addEventListener("resize", () => {
	listScrollWidth = list.scrollWidth;
});
const scrollBtns = document.querySelectorAll(".opportunities button.scroll");
function toggleBtn(btn, enable = true) {
	if (enable) {
		if (btn.getAttribute("disabled") == "false") return;
		btn.removeAttribute("disabled");
	} else {
		if (btn.getAttribute("disabled") == "true") return;
		btn.setAttribute("disabled", "true");
	}
}

scrollBtns.forEach((btn) => {
	btn.addEventListener("click", () => {
		if (btn.classList.contains("scroll-right")) {
			if (list.scrollLeft + SCROLL_STEP >= listScrollWidth) {
				list.scrollLeft = listScrollWidth;
				toggleBtn(btn, false);
				return;
			} else {
				toggleBtn(btn, true);
				toggleBtn(scrollBtns[0], true);
				list.scrollLeft += SCROLL_STEP;
			}
		} else {
			if (list.scrollLeft <= SCROLL_STEP) {
				list.scrollLeft = 0;
				toggleBtn(btn, false);
			} else {
				toggleBtn(btn, true);
				toggleBtn(scrollBtns[1], true);
				list.scrollLeft -= SCROLL_STEP;
			}
			console.log(list.scrollLeft);
		}
	});
});
// });

const markersByType = {
	agronegocio: [[-8.9775238, -48.1815862, "Pedro Afonso"]],
	alimentos: [[-7.2108598, -48.2585657, "Araguaína"]],
	auto: [
		[-7.3108598, -48.2585657, "auto"],
		[-8.9775238, -48.1815862, "Pedro Afonso"],
	],
	calcados: [[-7.4108598, -48.2585657, "calcados"]],
	comercio: [
		[-7.5108598, -48.2585657, "comercio"],
		[-8.9775238, -48.1815862, "Pedro Afonso"],
	],
	energias: [[-7.6108598, -48.2585657, "energias"]],
	hppc: [
		[-7.7108598, -48.2585657, "hppc"],
		[-8.9775238, -48.1815862, "Pedro Afonso"],
	],
	infra: [[-7.8108598, -48.2585657, "infra"]],
	mineracao: [
		[-7.9108598, -48.2585657, "mineracao"],
		[-8.9775238, -48.1815862, "Pedro Afonso"],
	],
	papel: [[-8.1108598, -48.2585657, "papel"]],
	petroleo: [
		[-8.2108598, -48.2585657, "petroleo"],
		[-8.9775238, -48.1815862, "Pedro Afonso"],
	],
	saude: [[-8.3108598, -48.2585657, "saude"]],
	turismo: [
		[-8.4108598, -48.2585657, "turismo"],
		[-8.9775238, -48.1815862, "Pedro Afonso"],
	],
};

let map;
let markers = [];
let image;
function initMap() {
	map = new google.maps.Map(document.querySelector(".map-wrapper"), {
		zoom: 6.5,
		center: { lat: -9.445, lng: -48.442 },
	});
	setMarkers(map);
}

const buttons = document.querySelectorAll(".opportunities__list button");
function clearButtons() {
	buttons.forEach((btn) => {
		btn.classList.remove("active");
	});
}
buttons.forEach((btn) => {
	btn.addEventListener("click", () => {
		clearMarkers();
		clearButtons();
		let type = btn.dataset.type;
		markersByType[type].forEach((m) => {
			addMarker(m[0], m[1], m[2]);
		});
		btn.classList.add("active");
	});
});

function setMarkers(map) {
	image = {
		url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
		size: new google.maps.Size(20, 32),
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(0, 32),
	};

	const shape = {
		coords: [1, 1, 1, 20, 18, 20, 18, 1],
		type: "poly",
	};

	// for (let i = 0; i < markers.length; i++) {
	// 	const marker = markers[i];

	// 	new google.maps.Marker({
	// 		position: { lat: marker[1], lng: marker[2] },
	// 		map,
	// 		icon: image,
	// 		shape: shape,
	// 		title: marker[0],
	// 		zIndex: marker[3],
	// 	});
	// }
}

function addMarker(latitude, longitude, title) {
	const marker = new google.maps.Marker({
		position: { lat: latitude, lng: longitude },
		map: map,
		title: title,
		icon: image,
	});
	markers.push(marker);
}

// Function to remove all markers from the map
function clearMarkers() {
	for (let i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}
	markers = [];
}

window.initMap = initMap();
