document.addEventListener("DOMContentLoaded", function () {
	var elems = document.querySelectorAll(".dropdown-trigger");
	var dropdowns = M.Dropdown.init(elems, {
		coverTrigger: false,
	});

	var elems = document.querySelectorAll(".sidenav");
	var mobileNav = M.Sidenav.init(elems);

	const fetchCurrencies = async () => {
		let fetchUrl = "https://economia.awesomeapi.com.br/last/USD-BRL,EUR-BRL,ARS-BRL";
		return await fetch(fetchUrl).then((response) => response.json());
	};

	const setCurrencyValue = (el, value, variation) => {
		if (parseFloat(variation) < 0) {
			el.classList.add("down");
		}
		el.textContent = value;
	};

	const showCurrencies = async () => {
		const currencies = await fetchCurrencies();
		const currencyValues = document.querySelectorAll(".markets .value");

		setCurrencyValue(currencyValues[0], currencies["USDBRL"].ask, currencies["USDBRL"].varBid);
		setCurrencyValue(currencyValues[1], currencies["EURBRL"].ask, currencies["EURBRL"].varBid);
		setCurrencyValue(currencyValues[2], currencies["ARSBRL"].ask, currencies["ARSBRL"].varBid);
	};
	try {
		showCurrencies();
	} catch (e) {}
});
