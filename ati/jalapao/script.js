document.addEventListener("DOMContentLoaded", function () {
	var elemsSidenav = document.querySelectorAll(".sidenav");
	var instancesSidenav = M.Sidenav.init(elemsSidenav, {});
	const slider = document.querySelector(".blaze-slider");
	new BlazeSlider(slider, {
		all: {
			enableAutoplay: false,
			autoplayInterval: 2000,
			transitionDuration: 300,
			slidesToShow: 1,
		},
	});
});
