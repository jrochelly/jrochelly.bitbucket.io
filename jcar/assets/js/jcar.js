document.addEventListener('DOMContentLoaded', function () {
    var announcePolite = document.querySelector(".announce-p");
    var announceAssertive = document.querySelector(".announce-a");
    var btnTriggerFilter = document.querySelector(".btn-trigger-filters");
    var menuBtn = document.querySelector("#menu-trigger");

    let tabs = document.querySelector(".tabs");
    var instanceTabs = M.Tabs.init(tabs);

    var menu = document.querySelector("#menu");

    menuBtn.addEventListener("click", function (e) {
        e.stopPropagation();
        menu.classList.toggle("open");
        if (menuBtn.getAttribute("aria-expanded") == "false") {
            menuBtn.setAttribute("aria-expanded", "true");
            menu.addEventListener("keydown", menuKeyboardHandler);
        } else {
            menuBtn.setAttribute("aria-expanded", "false");
            menuBtn.focus();
        }
    });

    function menuKeyboardHandler(e) {
        if (e.keyCode == 27) {
            menuBtn.setAttribute("aria-expanded", "false");
            menu.classList.remove("open");
        }
    }

    function closeDropdown() {
        menu.classList.remove("open");
        menuBtn.setAttribute("aria-expanded", "false");
    }

    document.addEventListener("click", function (event) {
        if (
            event.target.matches("#menu-trigger.open") ||
            !event.target.closest("#menu.open")
        ) {
            closeDropdown();
        }
    }, false);


    // Filtros
    var sliders = document.querySelectorAll(".preco_slider");

    sliders.forEach(slide => {
        slide.querySelector("input[type=range]").addEventListener("input", function () {
            slide.querySelector("input[type=number]").value = this.value;
        });
        slide.querySelector("input[type=number]").addEventListener("change", function () {
            slide.querySelector("input[type=range]").value = this.value;
        });
    });


    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);

});

function announceA(text) {
    announceAssertive.innerHTML = text;
}

function announceP(text) {
    announcePolite.innerHTML = text;
}