
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ["", "", { role: "style" } ],
          ["CARTÃO ABC", 8.94, "#6AA84F"],
          ["CARTÃO XYZ", 10.49, "#46BDC6"],
          ["CARTÃO JKL", 19.30, "#9900FF    "]
         
        ]);
  
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);
  
        var options = {
         
          chartArea: {width: '80%'},
          height: 400,
          bar: {groupWidth: "40%"},
          legend: { position: "none" },
        };

      var chart = new google.visualization.BarChart(document.getElementById('chart_cartao'));

      chart.draw(data, options);
    }
    
//portador

    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawPortador);
    function drawPortador() {
      var data = google.visualization.arrayToDataTable([
        ["Portador", "Valor", { role: "style" } ],
        ["Portador ABC", 8.94, "#6AA84F"],
        ["Portador XYZ", 10.49, "#46BDC6"],
        ["Portador JKL", 19.30, "#9900FF"],
        ["Portador ABC", 8.94, "#6AA84F"],
        ["Portador XYZ", 10.49, "#46BDC6"],
        ["Portador JKL", 19.30, "#9900FF"]      
       
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
       
        chartArea: {width: '80%'},
        height: 400,
        bar: {groupWidth: "30%"},
        legend: { position: "none" },
      };

    var chart = new google.visualization.BarChart(document.getElementById('chart_portador'));

    chart.draw(data, options);
  }

  // estabelecimento

  google.charts.setOnLoadCallback(estabelecimento);

function estabelecimento() {

  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Mês');
  data.addColumn('number', 'Orçamento');
  data.addRows([
    ['Janeiro', 167681.00],
    ['Fevereiro', 91812.04],
    ['Março', 48985.32],
    ['Abril', 37302.61],
    ['Maio', 26784.55],
    ['Junho', 26784.55],
    ['Julho', 26784.55],
    ['Agosto', 26784.55],
    ['Setembro', 26784.55],
    ['Outubro', 26784.55],
    ['Novembro', 26784.55],
    ['Dezembro', 26784.55]
  ]);
  var options = {
    chartArea: {
      right: 0,
      bottom: 30,
      width: '90%',
      height: '90%'
    },
    colors: ['#6AA84F'],
    height: 450,
    legend: 'none',
    bar: {
      groupWidth: '75%'
    },
    vAxis: {
      gridlines: {
        count: 6
      },
      format: 'currency'
    }
  };
  var formatter = new google.visualization.NumberFormat({
      prefix: 'R$',
      negativeColor: 'red',
      negativeParens: true
  });
  formatter.format(data, 1); // Apply formatter to second column
    

  var chart = new google.visualization.ColumnChart(document.getElementById('chart_estabelecimento'));
  chart.draw(data, options);

}