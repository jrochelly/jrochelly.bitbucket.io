google.load("visualization", "1", {
  packages: ["corechart", "bar"],
  'language': 'pt-br'
});
google.setOnLoadCallback(drawChartAtuacao);
google.setOnLoadCallback(drawChartCategoria);
google.setOnLoadCallback(drawChartOrigem);

function drawChartAtuacao() {
  var data = google.visualization.arrayToDataTable([
    ['Área', 'Porcentagem'],
    ['Saúde', 22],
    ['Educação', 9],
    ['Segurança', 9],
    ['Agricultura', 9],
    ['Cultura', 9],
    ['Transporte', 9]
  ]);
  var options = {
    title: "",
    chartArea: {
      left: 0,
      top: 0,
      width: '100%',
      height: '90%'
    },
    height: 350,
    pieHole: 0.5,
    pieSliceBorderColor: "none",
    legend: {
      position: "bottom"
    },
    tooltip: {
      trigger: "none"
    }
  };
  var chart = new google.visualization
    .PieChart(document.getElementById('despesaAtuacao'));
  chart.draw(data, options);
}

function drawChartCategoria() {
  var data = google.visualization.arrayToDataTable([
    ['Área', 'Porcentagem'],
    ['Saúde', 22],
    ['Educação', 9],
    ['Segurança', 9],
    ['Agricultura', 9],
    ['Cultura', 9],
    ['Transporte', 9]
  ]);
  var options = {
    title: "",
    chartArea: {
      left: 0,
      top: 0,
      width: '100%',
      height: '90%'
    },
    height: 350,
    pieHole: 0.5,
    pieSliceBorderColor: "none",
    legend: {
      position: "bottom"
    },
    tooltip: {
      trigger: "none"
    }
  };
  var chart = new google.visualization
    .PieChart(document.getElementById('despesaCategoria'));
  chart.draw(data, options);
}

function drawChartOrigem() {
  var data = google.visualization.arrayToDataTable([
    ['Área', 'Porcentagem'],
    ['Saúde', 22],
    ['Educação', 9],
    ['Segurança', 9],
    ['Agricultura', 9],
    ['Cultura', 9],
    ['Transporte', 9]
  ]);
  var options = {
    title: "",
    chartArea: {
      left: 0,
      top: 0,
      width: '100%',
      height: '90%'
    },
    height: 350,
    pieHole: 0.5,
    pieSliceBorderColor: "none",
    legend: {
      position: "bottom"
    },
    tooltip: {
      trigger: "none"
    }
  };
  var chart = new google.visualization
    .PieChart(document.getElementById('despesaOrigem'));
  chart.draw(data, options);
}


//Barras

google.charts.setOnLoadCallback(orcamentoMensal);

function orcamentoMensal() {

  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Mês');
  data.addColumn('number', 'Orçamento');
  data.addRows([
    ['Janeiro', 167681.00],
    ['Fevereiro', 91812.04],
    ['Março', 48985.32],
    ['Abril', 37302.61],
    ['Maio', 26784.55],
    ['Junho', 26784.55],
    ['Julho', 26784.55],
    ['Agosto', 26784.55],
    ['Setembro', 26784.55],
    ['Outubro', 26784.55],
    ['Novembro', 26784.55],
    ['Dezembro', 26784.55]
  ]);
  var options = {
    chartArea: {
      right: 0,
      bottom: 30,
      width: '90%',
      height: '90%'
    },
    colors: ['#F88631'],
    height: 450,
    legend: 'none',
    bar: {
      groupWidth: '75%'
    },
    vAxis: {
      gridlines: {
        count: 6
      },
      format: 'currency'
    }
  };
  var formatter = new google.visualization.NumberFormat({
      prefix: 'R$',
      negativeColor: 'red',
      negativeParens: true
  });
  formatter.format(data, 1); // Apply formatter to second column
    

  var chart = new google.visualization.ColumnChart(document.getElementById('despesaOrcamento'));
  chart.draw(data, options);

}