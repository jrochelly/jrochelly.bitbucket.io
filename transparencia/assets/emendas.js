google.load("visualization", "1", {
  packages: ["corechart"],
  'language': 'pt-br'
});
google.setOnLoadCallback(drawChartAtuacao);

function drawChartAtuacao() {
  var data = google.visualization.arrayToDataTable([
    ['Área', 'Porcentagem'],
    ['Saúde', 22],
    ['Educação', 9],
    ['Segurança', 9],
    ['Agricultura', 9],
    ['Cultura', 9],
    ['Transporte', 9]
  ]);
  var options = {
    title: "",
    chartArea: {
      left: 0,
      top: 0,
      width: '100%',
      height: '90%'
    },
    height: 350,
    pieHole: 0.5,
    pieSliceBorderColor: "none",
    legend: {
      position: "bottom"
    },
    tooltip: {
      trigger: "none"
    }
  };
  var chart = new google.visualization
    .PieChart(document.getElementById('emendasAtuacao'));
  chart.draw(data, options);
}

google.charts.setOnLoadCallback(gastosMunicipio);

  function gastosMunicipio() {
  
    var data = google.visualization.arrayToDataTable([
      ['Localidade', 'Qtd. Emendas'],
      ['Almas', 10],
      ['Palmas', 9],
      ['Gurupi', 5],
      ['Xambioá', 4],
      ['Araguína', 3],
      ['Filadélfia', 2]
      
    ]);
    var options = {
      chartArea: {
        right: 0,
        bottom: 30,
        width: '100%',
        height: '90%'
      },
      height: 350,
      legend: 'none',
      colors: ["#259551"],
      vAxis: { textPosition: 'none' },
      annotations: {
        highContrast: true,    
        textStyle: {
            fontName: 'Roboto',
            fontSize: 16,
        }
      },
      bar: {
        groupWidth: '45%'
      },
      
    };
    var formatter = new google.visualization.NumberFormat({
        prefix: '',
        negativeColor: 'red',
        negativeParens: true
    });
    formatter.format(data, 1); // Apply formatter to second column
      
  
    var chart = new google.visualization.ColumnChart(document.getElementById('gastosmunicipio'));
    chart.draw(data, options);
  
  }