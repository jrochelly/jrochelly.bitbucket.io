
 
   //receitas por Pie

 google.load("visualization", "1", {
    packages: ["corechart", "bar"],
    'language': 'pt-br'
  });
  
  google.setOnLoadCallback(drawChartCategoria);
  google.setOnLoadCallback(drawChartOrigem);
  google.setOnLoadCallback(drawChartPoder);
  
  function drawChartCategoria() {
    var data = google.visualization.arrayToDataTable([
      ['Área', 'Porcentagem'],
      ['Receitas Correntes', 22],
      ['Impostos', 9],
      ['Receitas de Capital', 9],
      ['Outra Categoria', 9],
      ['Outras', 15]
      
    ]);
    var options = {
      title: "",
      chartArea: {
        left: 0,
        top: 0,
        width: '100%',
        height: '90%'
      },
      height: 350,
      pieHole: 0.5,
      pieSliceBorderColor: "none",
      legend: {
        position: "bottom"
      },
      tooltip: {
        trigger: "none"
      }
    };
    var chart = new google.visualization
      .PieChart(document.getElementById('receitaCategoria'));
    chart.draw(data, options);
  }
  
  function drawChartOrigem() {
    var data = google.visualization.arrayToDataTable([
      ['Área', 'Porcentagem'],
      ['Transferências Correntes', 22],
      ['Impostos', 9],
      ['Taxas', 9],
      ['Contribuintes', 9],
      ['Outra Categoria', 9]
    ]);
    var options = {
      title: "",
      chartArea: {
        left: 0,
        top: 0,
        width: '100%',
        height: '90%'
      },
      height: 350,
      pieHole: 0.5,
      pieSliceBorderColor: "none",
      legend: {
        position: "bottom"
      },
      tooltip: {
        trigger: "none"
      }
    };
    var chart = new google.visualization
      .PieChart(document.getElementById('receitaOrigem'));
    chart.draw(data, options);
  }
  
  function drawChartPoder() {
    var data = google.visualization.arrayToDataTable([
      ['Área', 'Porcentagem'],
      ['Executivo', 75],
      ['Legislativo', 10],
      ['Judiciario', 15]
    ]);
    var options = {
      title: "",
      chartArea: {
        left: 0,
        top: 0,
        width: '100%',
        height: '90%'
      },
      height: 350,
      pieHole: 0.5,
      pieSliceBorderColor: "none",
      legend: {
        position: "bottom"
      },
      tooltip: {
        trigger: "none"
      }
    };
    var chart = new google.visualization
      .PieChart(document.getElementById('receitaPoder'));
    chart.draw(data, options);
  }
  
  
  
  
  // receita
  
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['mes', '2016 ', '2017', '2018', '2019', '2020'],
      ['Janeiro', 1000, 400, 1000, 400, 200],
      ['Fevereiro', 1170, 460, 1000, 400, 250],
      ['Março', 660, 1120, 1000, 400, 300],
      ['Abril', 660, 1120, 1000, 400, 300],
      ['Maio', 1030, 540, 1000, 400, 350],
      ['Abril', 660, 1120, 1000, 400, 300],
      ['Maio', 1000, 400, 1000, 400, 200],
      ['Junho', 1170, 460, 1000, 400, 250],
      ['Julho', 660, 1120, 1000, 400, 300],
      ['Agosto', 660, 1120, 1000, 400, 300],
      ['Setembro', 1030, 540, 1000, 400, 350],
      ['Outubro', 660, 1120, 1000, 400, 300],
      ['Novembro', 1000, 400, 1000, 400, 200],
      ['Dezembro', 1170, 460, 1000, 400, 250]
    ]);

    var options = {
      legend: {position: 'bottom'},
      chartArea: {
        height: '80%'
      },
      height: 450
     
    };

    var chart = new google.charts.Bar(document.getElementById('chart_liquida'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
  }



  