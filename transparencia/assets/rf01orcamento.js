google.load("visualization", "1", {
  packages: ["corechart", "bar"],
  'language': 'pt-br'
});
google.setOnLoadCallback(drawChartAtuacao);
google.setOnLoadCallback(drawChartOrigem);

function drawChartAtuacao() {
  var data = google.visualization.arrayToDataTable([
    ['Área', 'Porcentagem'],
    ['Impostos', 22],
    ['Receita Patrimonial', 9],
    ['Outros', 9]
  ]);
  var options = {
    title: "",
    chartArea: {
      left: 0,
      top: 0,
      width: '100%',
      height: '90%'
    },
    colors: ['#2E5790', '#4684DB', '#647EA2', '#88ABDB', '#1D375C'],
    height: 350,
    pieHole: 0.5,
    pieSliceBorderColor: "none",
    legend: {
      position: "bottom"
    },
    tooltip: {
      trigger: "none"
    }
  };
  var chart = new google.visualization
    .PieChart(document.getElementById('receitaAtuacao'));
  chart.draw(data, options);
}

function drawChartOrigem() {
  var data = google.visualization.arrayToDataTable([
    ['Área', 'Porcentagem'],
    ['Despesas', 22],
    ['Receita', 9]
  ]);
  var options = {
    colors: ['#D92E2E', '#4684DB'],
    title: "",
    chartArea: {
      left: 0,
      top: 0,
      width: '100%',
      height: '90%'
    },
    height: 350,
    pieHole: 0.5,
    pieSliceBorderColor: "none",
    legend: {
      position: "bottom"
    },
    tooltip: {
      trigger: "none"
    }
  };
  var chart = new google.visualization
    .PieChart(document.getElementById('receitaOrigem'));
  chart.draw(data, options);
}


//linhas

google.charts.load('current', {'packages':['line']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

var data = new google.visualization.DataTable();
data.addColumn('string', 'Mês');
data.addColumn('number', 'Receita');
data.addColumn('number', 'Despesa');


data.addRows([
  ['Janeiro',  37.8, 80.8],
  ['Fevereiro',  30.9, 69.5],
  ['Março',  25.4,   57],
  ['Abril',  11.7, 18.8],
  ['Maio',  11.9, 17.6],
  ['Junho',   8.8, 13.6],
  ['Julho',   7.6, 12.3],
  ['Agosto',  12.3, 29.2],
  ['Setembro',  16.9, 42.9],
  ['Outubro', 12.8, 30.9],
  ['Novembro',  5.3,  7.9],
  ['Dezembro',  6.6,  8.4]
]);

var options = {
  chart: {
    title: '',
    subtitle: ''
  },
  chartArea:{width:'100%',height:'80%'},
  hAxis: {
    textPosition: 'none',
    title: ''
  },
  legend: { position: 'none' },
  colors: [ '#4684DB', '#D92E2E'],
  height: 500,
  format: 'currency'
};
var formatter = new google.visualization.NumberFormat({
  prefix: 'R$',
  negativeColor: 'red',
  negativeParens: true
});
formatter.format(data, 1); // Apply formatter to primary column
formatter.format(data, 2); // Apply formatter to second column


var chart = new google.charts.Line(document.getElementById('evolucao'));

chart.draw(data, google.charts.Line.convertOptions(options));
}
