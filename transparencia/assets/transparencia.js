$(document).ready(function () {
    $(".monthpicker").Monthpicker();

    $(".btn-exercicio").on("click", function(e){
        $(this).toggleClass("active");
        $(".exercicio-date-picker").toggleClass("show-date-picker");
        e.stopPropagation();
    });

    $(".page-filtros--head").on("click", function(){
        $(".page-filtros").toggleClass("open");
    });

    // Limpar filtros
    $(".btn-limpar-filtros").on("click", function(){
        // Definir Limpeza de filtros e submit
    });
    // Submit filtro datepicker
    $(".btn-submit-date-picker").on("click", function(){
        // Definir form.submit();
    });

    $("select").formSelect();
    // $(".menus .submenu-item").css("top", $(".menu-item#pessoal").offset().top + 200);
    // $(".menus .submenu-item").css("left", $(".menu-item#pessoal").offset().left - 1);

    // $(".mini-menus .submenu-item").css("top", $(".menu-item#pessoal").offset().top - 20);
    // $(".mini-menus .submenu-item").css("left", $(".menu-item#pessoal").offset().left - 1);

    $(".page-help").on("mouseover", function(){
        $("page-help--box").addClass("hovered");
    });

    $(".page-help").on("mouseover", function(){
        $("page-help--box").addClass("hovered");
    });

    $(".menus .menu-item--title").on("click", function (e) {
        var menuId = $(this).parent().attr("id");
        if ($(this).parent().hasClass("menu-active")) {
            e.preventDefault();
            $(this).parent().removeClass("menu-active");
            $(".submenu-item#" + menuId).removeClass("show-submenu");
        } else {
            $(".menu-item").removeClass("menu-active");
            $(this).parent().toggleClass("menu-active");
            
            $(".submenu-item").removeClass("show-submenu");
            var submenuItemPos = ($(".submenus").offset().top - $(".menu-item#" + menuId).offset().top) - 150;
            $(".submenu-item#" + menuId).css("top", submenuItemPos * -1);
            $(".submenu-item#" + menuId).toggleClass("show-submenu");
            $('body').animate({
                scrollTop: $(".menu-item.menu-active").position().top + $("body").scrollTop() - 10
            }, 300);
                    
        }
        e.stopPropagation();
    });

    $(".mini-menus .menu-item--title").on("click", function (e) {
        var menuId = $(this).parent().attr("id");
        if ($(this).parent().hasClass("menu-active")) {
            e.preventDefault();
            $(this).parent().removeClass("menu-active");
            $(".submenu-item#" + menuId).removeClass("show-submenu");
        } else {
            $(".menu-item").removeClass("menu-active");
            $(this).parent().toggleClass("menu-active");

            $(".submenu-item").removeClass("show-submenu");
            $(".submenu-item#" + menuId).toggleClass("show-submenu");

            // $(".submenu-item#" + menuId).css("top", 10);
            // $(".submenu-item").css("left", $(".menu-item#pessoal").offset().left - 1);
        }
        e.stopPropagation();
    });

    $(".menu-btn").on("click", function () {
        $("li.mobile-submenu-open").removeClass("mobile-submenu-open");
        $('.mobile-menu').addClass("show-menu");
    });

    $('.mobile-menu li:has(ul)').addClass("has-children");
    $(".mobile-menu li").on("click", function () {
        if ($(this).children("ul").length) {
            $("li.mobile-submenu-open").removeClass("mobile-submenu-open");
            $(this).addClass("mobile-submenu-open");
        }
    });

    $(".submenu-item--close-btn").on("click", function(){
        $(".menu-item").removeClass("menu-active");
        $(".submenu-item").removeClass("show-submenu");
    });

    $(document).on("click", function (e) {
        var menu = $(".mobile-menu");
        var menubtn = $(".menu-btn");
        var submenu = $(".submenu-item");
        var btnexercicio = $(".btn-exercicio");
        var exerciciodatepicker = $(".exercicio-date-picker");
        

        if (menu.hasClass("show-menu")
            && !menu.is(e.target)
            && menubtn.has(e.target).length === 0
            && menu.has(e.target).length === 0
        ) {
            e.preventDefault();
            menu.removeClass("show-menu");
        }
        if (submenu.hasClass("show-submenu")
            && !submenu.is(e.target)
            && submenu.has(e.target).length === 0
        ) {
            e.preventDefault();
            submenu.removeClass("show-submenu");
            $(".menu-item").removeClass("menu-active");
        }
        if (exerciciodatepicker.hasClass("show-date-picker")
            && !exerciciodatepicker.is(e.target)
            && btnexercicio.has(e.target).length === 0
            && exerciciodatepicker.has(e.target).length === 0
        ) {
            e.preventDefault();
            exerciciodatepicker.removeClass("show-date-picker");
            btnexercicio.removeClass("active");
        }
    });


    // Accordion Grafico
    $(".ref-tab").click(function(event){
        $(this).children(".acordion").slideToggle("");
        $(this).toggleClass("open");
      });

      $('.modal').modal();
      
});