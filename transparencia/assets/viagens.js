google.charts.load('current', { 'packages': ['corechart'] });
  //Barras
  
  google.charts.setOnLoadCallback(servidoresMaisViajaram);

  function servidoresMaisViajaram() {
  
    var data = google.visualization.arrayToDataTable([
      ['Servidore', 'Qtd. viagens'],
      ['Fulando de GHI', 10],
      ['Fulando de XYZ', 9],
      ['Fulando de BNM', 5],
      ['Fulando de RTZ', 4],
      ['Fulando de XCV', 3],
      ['Fulando de ABC', 2]
      
    ]);
    var options = {
      chartArea: {
        right: 0,
        bottom: 30,
        width: '90%',
        height: '90%'
      },
      height: 450,
      legend: 'none',
      colors: ["#3670BC"],
      bar: {
        groupWidth: '45%'
      },
      
    };
    var formatter = new google.visualization.NumberFormat({
        prefix: '',
        negativeColor: 'red',
        negativeParens: true
    });
    formatter.format(data, 1); // Apply formatter to second column
      
  
    var chart = new google.visualization.BarChart(document.getElementById('servidoresmaisviajaram'));
    chart.draw(data, options);
  
  }


  google.charts.setOnLoadCallback(gastosUF);

  function gastosUF() {
  
    var data = google.visualization.arrayToDataTable([
      ['UF', 'Qtd. viagens',{role: 'annotation'}],
      ['Tocantins', 10, 'Tocantins'],
      ['Pernambuco', 9, 'Pernambuco'],
      ['Maranhão', 5, 'Maranhão'],
      ['São Paulo', 4, 'São Paulo'],
      ['Amazonas', 3, 'Amazonas'],
      ['Mato Grosso', 2, 'Mato Grosso']
      
    ]);
    var options = {
      chartArea: {
        right: 0,
        bottom: 30,
        width: '100%',
        height: '90%'
      },
      height: 450,
      legend: 'none',
      colors: ["#259551"],
      vAxis: { textPosition: 'none' },
      annotations: {
        highContrast: true,    
        textStyle: {
            fontName: 'Roboto',
            fontSize: 16,
        }
      },
      bar: {
        groupWidth: '45%'
      },
      
    };
    var formatter = new google.visualization.NumberFormat({
        prefix: '',
        negativeColor: 'red',
        negativeParens: true
    });
    formatter.format(data, 1); // Apply formatter to second column
      
  
    var chart = new google.visualization.BarChart(document.getElementById('gastosuf'));
    chart.draw(data, options);
  
  }

  google.charts.setOnLoadCallback(gastosInternacionais);

  function gastosInternacionais() {
  
    var data = google.visualization.arrayToDataTable([
      ['Cidade', 'Qtd. viagens',{role: 'annotation'}],
      ['Florida, EUA', 10, 'Florida, EUA'],
      ['Paris, FR', 9, 'Paris, FR'],
      ['Macau, CN', 5, 'Macau, CN'],
      ['Pequim, CN', 4, 'Pequim, CN'],
      ['Açores, PT', 3, 'Açores, PT'],
      ['Tel Aviv, IL', 2, 'Tel Aviv, IL']
      
    ]);
    var options = {
      chartArea: {
        right: 0,
        bottom: 30,
        width: '100%',
        height: '90%'
      },
      height: 450,
      legend: 'none',
      colors: ["#8C55CB"],
      vAxis: { textPosition: 'none' },
      annotations: {
        highContrast: true,    
        textStyle: {
            fontName: 'Roboto',
            fontSize: 16,
        }
      },
      bar: {
        groupWidth: '45%'
      },
      
    };
    var formatter = new google.visualization.NumberFormat({
        prefix: '',
        negativeColor: 'red',
        negativeParens: true
    });
    formatter.format(data, 1); // Apply formatter to second column
      
  
    var chart = new google.visualization.BarChart(document.getElementById('gastosinternacionais'));
    chart.draw(data, options);
  
  }

  google.charts.setOnLoadCallback(gastosOrgaos);

  function gastosOrgaos() {
  
    var data = google.visualization.arrayToDataTable([
      ['Órgão', 'Gastos',{role: 'annotation'}],
      ['Secretaria do Meio Ambiente', 56321.00, 'Secretaria do Meio Ambiente'],
      ['Secretaria da Administração', 50321.00, 'Secretaria da Administração'],
      ['Secretaria da Fazenda', 48321.00, 'Secretaria da Fazenda'],
      ['Secretaria da Segurança Pública', 45321.00, 'Secretaria da Segurança Pública'],
      ['Naturatins', 33321.00, 'Naturatins'],
      ['Secretaria da Saúde', 22321.00, 'Secretaria da Saúde']
      
    ]);
    var options = {
      chartArea: {
        right: 0,
        bottom: 30,
        width: '100%',
        height: '90%'
      },
      height: 450,
      legend: 'none',
      vAxis: { textPosition: 'none' },
      annotations: {
        highContrast: true,    
        textStyle: {
            fontName: 'Roboto',
            fontSize: 16,
        }
      },
      colors: ["#F3B447"],
      bar: {
        groupWidth: '45%'
      },
      
    };
    var formatter = new google.visualization.NumberFormat({
        prefix: '',
        negativeColor: 'red',
        negativeParens: true
    });
    formatter.format(data, 1); // Apply formatter to second column
      
  
    var chart = new google.visualization.BarChart(document.getElementById('gastosorgaos'));
    chart.draw(data, options);
  
  }
