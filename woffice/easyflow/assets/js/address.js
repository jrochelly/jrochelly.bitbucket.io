import { updateFlowData } from "./common.js";

document.addEventListener("DOMContentLoaded", function () {
	const center = { lat: 50.064192, lng: -130.605469 };
	// Create a bounding box with sides ~10km away from the center point
	const defaultBounds = {
		north: center.lat + 0.1,
		south: center.lat - 0.1,
		east: center.lng + 0.1,
		west: center.lng - 0.1,
	};
	const input = document.getElementById("address_autocomplete");
	const options = {
		bounds: defaultBounds,
		componentRestrictions: { country: "us" },
		fields: ["address_components", "geometry", "icon", "name"],
		strictBounds: false,
		types: ["establishment"],
	};
	const autocomplete = new google.maps.places.Autocomplete(input, options);

	// Specify data fields to avoid being billed for Places Data SKUs you don't need.
	// Include the fields property in the AutocompleteOptions that are passed to the widget constructor,
	// as demonstrated in the previous example, or call setFields() on an existing Autocomplete object.
	// autocomplete.setFields(["place_id", "geometry", "name"]);

	google.maps.event.addListener(autocomplete, "place_changed", function () {
		let addressComplete = document.querySelector("#address_complete");
		let addressVisible = document.querySelector("#address_visible");
		let searchplace = autocomplete.getPlace();

		let address = searchplace.address_components.reduce(
			(prev, curr, currIdx) => {
				if (currIdx === 1) {
					return `${prev.long_name} ${curr.long_name}`;
				}
				return `${prev} ${curr.long_name}`;
			},
		);

		let mapOject = {
			...searchplace,
			lat: searchplace.geometry.location.lat(),
			lng: searchplace.geometry.location.lng(),
		};

		updateFlowData({ map: mapOject });

		console.log(mapOject);

		addressComplete.value = addressVisible.textContent = address;

		const section = document.querySelector(`section#address`);
		let position = section.getBoundingClientRect();
		// scrolls to 20px above element
		window.scrollTo(position.left, position.top + window.scrollY - 20);
	});
});
