import { getflowInputValues } from "./common.js";

let instancesAC;
const clientsData = [{}];
let optionsAC = {
	data: {},
	onAutocomplete: function (e) {
		let cData = getClient(e);
	},
};

const getClient = (name = "") => {
	let client = clientsData.filter((client) => client.name === name)[0];
	// Set data to inputs
	let clientPhone = document.querySelector("#client_phone");
	let clientEmail = document.querySelector("#client_email");
	let clientId = document.querySelector("#client_id");
	let clientCategory = document.querySelector(
		`.client_category[value="${client.category}"]`,
	);

	clientId.value = client.id;
	clientPhone.value = client.phone;
	clientEmail.value = client.email;
	clientCategory.setAttribute("checked", true);
	// Update flowData
	getflowInputValues();
};

/**
 * Fetch all clients
 * @param {string} query Client's name
 * @returns array of clients
 */
const fetchClients = (query = "") => {
	let autocompletes = document.querySelectorAll(".autocomplete");
	// TODO: Add QUERY param to filter names instead fetching all collection
	const result = fetch(`assets/js/fakedata/clients.json?query=${query}`)
		.then((res) => res.json())
		.then((clients) => {
			// Assign fetched data to global scope const
			Object.assign(clientsData, clients);
			const clientsNames = clients.reduce((clients, currentClient) => {
				return {
					...clients,
					[currentClient["name"]]: null,
				};
			}, {});
			optionsAC = {
				...optionsAC,
				data: clientsNames,
			};
			instancesAC = M.Autocomplete.init(autocompletes, optionsAC);
			return clients;
		});
	return result;
};

document.addEventListener("DOMContentLoaded", function () {
	// TODO: make it on key pressed
	const clients = fetchClients();
	const clientName = document.querySelector("#client_name");

	clientName.addEventListener("change", () => {
		setTimeout(() => {
			M.updateTextFields();
		}, 100);
	});
});
