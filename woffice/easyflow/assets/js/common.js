export let flowData = {
	client: {},
	address: {},
	measures: [{}],
	products: [{}],
	tax: 0.0,
};

// Used outside this function
export const updateFlowData = (newData) => {
	flowData = {
		...flowData,
		...newData,
	};
	console.log("newflow", flowData);
	localStorage.setItem("flowData", JSON.stringify(flowData));
};

/**
 * Gatter all data from inputs, updates flowData variable
 * and sidebar display content
 */
export const getflowInputValues = () => {
	const allInputs = document.querySelectorAll(".woffice-easy-flow input");
	let inputValues = [];
	let inputsClient = [];
	let inputsAddress = [];
	let inputsTaxes = [];
	let inputsProducts = [];

	allInputs.forEach((input) => {
		if (input.type === "radio" && !input.checked) {
			return;
		}
		let children = input.name.split("_");
		if (children.length > 1) {
			switch (children[0]) {
				case "client": {
					inputsClient = {
						...inputsClient,
						[children[1]]: input.value,
					};
					break;
				}
				case "address": {
					inputsAddress = {
						...inputsAddress,
						[children[1]]: input.value,
					};
					break;
				}
				case "measures": {
					break;
				}
				case "products": {
					inputsProducts = {
						...inputsProducts,
						[`product${children[1]}`]: {
							...inputsProducts[`product${children[1]}`],
							[children[2]]: input.value,
						},
					};
					break;
				}
				case "tax": {
					inputsTaxes = {
						...inputsTaxes,
						[children[1]]: input.value.replace(/['$'\s]/gi, ""),
					};
					break;
				}
			}
			return;
		}
		if (input.type === "radio" && !input.checked) {
			return;
		}
		inputValues = {
			...inputValues,
			[input.name]: input.value,
		};
	});

	flowData = {
		...flowData,
		...inputValues,
		client: { ...inputsClient },
		address: { ...inputsAddress },
		taxes: { ...inputsTaxes },
		products: { ...inputsProducts },
	};

	// console.log(flowData);
};

const updateSummaryElement = (element, value = "") => {
	let el = document.querySelector(`span.${element}`);
	el.innerText = value;
};

document.addEventListener("DOMContentLoaded", function () {
	const allInputs = document.querySelectorAll(
		".woffice-easy-flow input:not([type='radio']):not([type='checkbox'])",
	);
	const allRadiosChecks = document.querySelectorAll(
		".woffice-easy-flow input[type=radio], .woffice-easy-flow input[type=checkbox]",
	);
	allInputs.forEach((input) => {
		input.addEventListener("blur", function (e) {
			getflowInputValues();
			M.updateTextFields();
		});
	});
	allRadiosChecks.forEach((input) => {
		input.addEventListener("click", function () {
			getflowInputValues();
		});
	});

	const finishBtn = document.querySelector("#finish-btn");
	finishBtn.addEventListener("click", (e) => {
		e.preventDefault();
		console.table(flowData);
		document.querySelector("form#easy-flow-form").submit();
	});

	var Selects = document.querySelectorAll("select");
	var instancesSelects = M.FormSelect.init(Selects);
});
