import { flowData, getflowInputValues } from "./common.js";

/**
 * Turn section visible and focus on the section's
 * first input/textarea/select element
 * @param {string} sectionId target section id attribute
 */
const focusSectionField = (sectionId) => {
	const nextSection = document.querySelector(`section#${sectionId}`);
	nextSection.classList.add("active");
	setTimeout(function () {
		let allFields = document.querySelectorAll(
			`section#${sectionId} input:not([type='hidden']), 
			 section#${sectionId} textarea:not([type='hidden']), 
			 section#${sectionId} select:not([type='hidden'])`,
		);

		if (document.body.clientWidth > 600) {
			window.scrollTo(0, nextSection.offsetTop - 120);
		} else {
			window.scrollTo(0, nextSection.offsetTop - 40);
		}
		// try {
		// 	allFields[0].focus();
		// } catch {}
	}, 100);
};

/**
 * Handle behavior on jumping to next section
 * @param {HTMLElement} btn Button used to jump to next section
 * @param {boolean} removeBtn Remove clicked button element if true
 */
const handleNextSectionClick = (btn, removeBtn = false) => {
	let sectionParent = btn.parentElement;
	while (sectionParent.nodeName !== "SECTION") {
		sectionParent = sectionParent.parentElement;
	}

	let sectionValitidy = true;
	sectionParent.querySelectorAll("input").forEach((input) => {
		if (input.checkValidity() === false) sectionValitidy = false;
	});

	if (sectionValitidy === false) {
		btn.classList.add("shake-animation");
		btn.parentElement.classList.add("invalid-fields");
		setTimeout(() => {
			btn.classList.remove("shake-animation");
		}, 300);
		return;
	}

	getflowInputValues();

	let nextSectionId = btn.getAttribute("data-next");

	if (nextSectionId === "deposit-question" && flowData.welcome === "1") {
		nextSectionId = "notes";
	}

	btn.parentElement.classList.remove("invalid-fields");
	focusSectionField(nextSectionId);
	if (removeBtn) {
		btn.remove();
	}
};

/**
 * Controls the behavior of section where a selection why prompt a text input
 * @param {string} replyId id of reply target
 */
const handleAskReplySelection = (replyElement = "") => {
	getflowInputValues();

	let dataNext = replyElement.getAttribute("data-next");
	if (dataNext) {
		focusSectionField(dataNext);
		return;
	}
	let replyId = replyElement.getAttribute("data-reply-target");

	const replies = document.querySelectorAll(".reply");
	replies.forEach((reply) => {
		reply.classList.remove("active");
		let rowFlowReplies = document.querySelector(".flow-replies");
		let flowNext = reply.parentNode.parentNode.querySelector(".flow-next");
		rowFlowReplies.classList.remove("active");
		flowNext.classList.remove("active");
		if (reply.id === replyId) {
			reply.classList.add("active");
			rowFlowReplies.classList.add("active");
			let amountInput = reply.querySelector("input");
			VMasker(amountInput).maskMoney({ unit: "$" });
			setTimeout(() => {
				amountInput.focus();
			}, 50);
			flowNext.classList.add("active");
		}
	});
};

document.addEventListener("DOMContentLoaded", function () {
	const nextBtns = document.querySelectorAll(".flow-next button.flow-btn");
	const twoOptionsInputs = document.querySelectorAll(
		".flow-step.two-options input",
	);
	const askReplyInputs = document.querySelectorAll(
		".flow-step.ask-reply .flow-ask input",
	);

	twoOptionsInputs.forEach((inputOption) => {
		inputOption.addEventListener("change", function (e) {
			handleNextSectionClick(inputOption);
		});
		inputOption.addEventListener("click", function (e) {
			handleNextSectionClick(inputOption);
		});
	});

	nextBtns.forEach((btn) =>
		btn.addEventListener("click", function () {
			handleNextSectionClick(btn, true);
		}),
	);

	askReplyInputs.forEach((input) =>
		input.addEventListener("click", function () {
			handleAskReplySelection(input);
		}),
	);

	// window.addEventListener("resize", () => {
	// 	currElem = document.activeElement;
	// 	setTimeout(() => {
	// 		currElem.scrollIntoView();
	// 	}, 300);
	// });
});
