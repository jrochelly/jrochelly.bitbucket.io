import { flowData, updateFlowData } from "./common.js";

document.addEventListener("DOMContentLoaded", function () {
	let measuresData = [];
	const firstMeasure = document.querySelector(
		"section#measures .measure:first-of-type",
	);
	const measuresList = document.querySelector(
		"section#measures .measures-list",
	);
	const btnAddMeasure = document.querySelector(".btn-add-measure");
	let btnDeleteMeasure = document.querySelectorAll(".btn-delete-measure");
	const btnNextSectionAtMeasure = document.querySelector(
		"#measures button.flow-btn",
	);
	const measures = document.querySelectorAll("section#measures .measure");

	// Add measures to the flowData when clicked on next btn
	btnNextSectionAtMeasure.addEventListener("click", function (e) {
		e.preventDefault();

		measures.forEach((m, i) => {
			let measureArea = {};
			let inputs = m.querySelectorAll("input");
			// INFO: inputs must have different names otherwise they will overwrite each other
			inputs.forEach((input) => {
				let keyName = input.name.split("_")[1];
				measureArea = {
					...measureArea,
					[keyName]: input.value,
				};
			});
			measuresData.push(measureArea);
		});
		updateFlowData({ measures: measuresData });
	});

	btnAddMeasure.addEventListener("click", function (e) {
		e.preventDefault();
		// FIXME: Temporary way of adding new measure
		// Should get from a partial
		let addMeasure = firstMeasure.cloneNode(true);

		addMeasure.classList.add("adding");
		addMeasure.classList.remove("deleting");
		measuresList.appendChild(addMeasure);
		setTimeout(() => {
			addMeasure.classList.remove("adding");
		}, 300);
		handleDeleteMeasure();
	});

	const handleDeleteMeasure = () => {
		btnDeleteMeasure = document.querySelectorAll(".btn-delete-measure");
		btnDeleteMeasure.forEach((btn) => {
			btn.addEventListener("click", function (e) {
				e.preventDefault();
				let deleteMeasure = e.target.parentElement;
				deleteMeasure.classList.add("deleting");
				setTimeout(() => {
					deleteMeasure.remove();
				}, 300);
			});
		});
	};
});
