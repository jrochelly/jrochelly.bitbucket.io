document.addEventListener("DOMContentLoaded", () => {
	var taxValue = document.querySelector("#tax_value");
	var discount = document.querySelector("#discount");
	VMasker(taxValue).maskMoney({ unit: "$" });
	VMasker(discount).maskMoney({ unit: "$" });
});
