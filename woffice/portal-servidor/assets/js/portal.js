document.addEventListener("DOMContentLoaded", function () {
	let dropElems = document.querySelectorAll(".dropdown-trigger");
	let dropInstances = M.Dropdown.init(dropElems, {
		coverTrigger: false,
		constrainWidth: false,
	});
	let sidenavElems = document.querySelectorAll(".sidenav");
	let sidenavInstances = M.Sidenav.init(sidenavElems);

	let collapsibleElems = document.querySelectorAll(".collapsible");
	let collapsInstances = M.Collapsible.init(collapsibleElems);

	const headerTriggers = document.querySelectorAll(".header-trigger");
	let headerActionsSearch = document.querySelector(".header-actions .search");
	let headerActionsUser = document.querySelector(".header-actions .user");

	const hideHeaderFloatingActions = (triggerToIgnore = undefined) => {
		headerTriggers.forEach((trigger) => {
			if (trigger === triggerToIgnore) return;
			trigger.parentElement.classList.remove("show");
		});
	};

	headerTriggers.forEach((trigger) => {
		trigger.addEventListener("click", (e) => {
			e.preventDefault();
			e.stopPropagation();
			trigger.parentElement.classList.toggle("show");
			hideHeaderFloatingActions(trigger);
		});
	});

	document.addEventListener("click", (e) => {
		if (
			e.target !== headerActionsSearch &&
			e.target !== headerActionsUser &&
			!e.target.closest(".show")
		) {
			if (
				[...headerActionsSearch.classList].includes("show") ||
				[...headerActionsUser.classList].includes("show")
			) {
				hideHeaderFloatingActions();
			}
		}
	});
});
