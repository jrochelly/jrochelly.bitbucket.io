document.addEventListener("DOMContentLoaded", function () {
	var collapsibles = document.querySelectorAll(".collapsible");
	var clpsinstance = M.Collapsible.init(collapsibles);
	var sidenav = document.querySelectorAll(".sidenav");
	var sninstance = M.Sidenav.init(sidenav);
	scrollAnimation();
});

if (typeof Tooltip !== 'undefined'){
	var tooltip = new Tooltip({
		theme: "light", // Selects one of the pre-defined tooltip styles - light or dark.
		distance: 5,    // Specifies the distance in pixels from trigger to tooltip.
		delay: 0        // Specifies how long the tooltip remains visible (in ms) after the mouse leaves the trigger.
	});
}

const scrollAnimation = () => {
	let imgsToAnimate = document.querySelectorAll(".animate-image");
	const winHeight = window.innerHeight;

	window.addEventListener("resize", function () {
		if (window.innerWidth <= 1440) {
			imgsToAnimate.forEach((img) => {
				img.style.transform = "translateX(0px)";
				img.style.opacity = 1;
			});
			return;
		}
	});

    const topics = document.querySelectorAll("[data-topic]");
    topics.forEach(topic => {
        topic.addEventListener("click", function(e){
            e.preventDefault();
            let section = document.querySelector(`.policy-grid-topic#${topic.dataset.topic}`)
            window.scrollTo(0, section.offsetTop - 96)
        })
    })

	document.addEventListener("scroll", function () {
		
		if (topics.length > 0) {
			const topicSections = document.querySelectorAll(".policy-grid-topic");
			let offset = window.pageYOffset;
			topicSections.forEach((ts) => {
				var section = document.getElementById(ts.id);
				var bounding = section.getBoundingClientRect();

				if (
					bounding.top <= window.innerHeight * 0.55 &&
					bounding.bottom >= window.innerHeight * 0.55
				) {
					let topic = document.querySelector(
						`.policy-grid-sidebar-content li[data-topic=${ts.id}]`,
					);
					topics.forEach((t) => {
						t.classList.remove("active");
					});
					topic.classList.add("active");
				}
			});
		}

		if (window.innerWidth <= 1440 || imgsToAnimate.length == 0) return;
		imgsToAnimate.forEach((img) => {
			let offset = window.pageYOffset;
			if (offset > img.offsetTop - winHeight) {
				let diff = img.offsetTop - (offset + 500);
				// Calc
				let tl = diff >= 0 ? diff : 0;
				let opc = diff >= 0 ? ((diff - 250) / 100) * -1 : 1;
				// Verify if has inverted animation
				if (img.classList.contains("invert")) {
					tl *= -1;
				}

				// Set position and opacity
				img.style.transform = `translateX(${tl}px)`;
				img.style.opacity = opc;
			}
		});
	});
};
