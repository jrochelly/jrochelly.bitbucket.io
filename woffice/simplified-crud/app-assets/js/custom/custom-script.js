/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 5.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */
$(document).ready(function(){

$(".select2").select2({
    dropdownAutoWidth: true,
    width: '100%',
    placeholder: 'Choose'
});


$(".save-estimate").on("click", function(e){
    e.preventDefault();
    swal({
        title: "Do you want to enter the measurements?",
        text: "",
        icon: 'info',
        buttons: {
            cancel: 'No',
            confirm: 'Yes, I want!'
        }
    }).then(function (result) {
    if (result) {
      location.href="estimate-measurement.html";
    } else {
      location.href="estimate-list.html";
    }
  });

});

});