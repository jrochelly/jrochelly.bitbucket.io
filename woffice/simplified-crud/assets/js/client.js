import { updateSmpData } from "./common.js";

document.addEventListener("DOMContentLoaded", () => {
	let clientId = document.querySelector("#client_id");
	const config = {
		// API Advanced Configuration Object
		selector: "#client_name",
		placeHolder: "Find a client...",
		data: {
			src: async (query) => {
				try {
					// Fetch Data from external Source
					const source = await fetch(`assets/js/fakedata/clients.json?query=${query}`);
					// Data should be an array of `Objects` or `Strings`
					const data = await source.json();
					// console.log(data);

					return data;
				} catch (error) {
					return error;
				}
			},
			// Data source 'Object' key to be searched
			keys: ["id", "name"],
		},
		resultsList: {
			element: (list, data) => {
				const message = document.createElement("li");
				message.setAttribute("class", "add_item");
				message.setAttribute("data-trigger", "modal-client");
				message.setAttribute("data-client-name", data.query);
				// console.log("data", data.results);
				// message.setAttribute("data-id", data.id);
				message.innerHTML = `<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" clip-rule="evenodd" d="M12 0C10.8954 0 10 0.89543 10 2V10H2C0.895431 10 0 10.8954 0 12C0 13.1046 0.89543 14 2 14H10V22C10 23.1046 10.8954 24 12 24C13.1046 24 14 23.1046 14 22V14H22C23.1046 14 24 13.1046 24 12C24 10.8954 23.1046 10 22 10H14V2C14 0.895431 13.1046 0 12 0Z" fill="currentColor"/>
				</svg>
				Add <mark>${data.query}</mark> as a new client`;
				list.append(message);
			},
			noResults: true,
		},
		resultItem: {
			highlight: true,
		},
		events: {
			input: {
				selection: (event) => {
					// TODO: change ID
					let nextListId = autoCompleteJS.data.store.length + 1;

					if (event.detail.event.target.classList.contains("add_item")) {
						event.preventDefault();
						// Capture modal trigger
						openModal(
							event.detail.event.target.dataset.trigger,
							event.detail.event.target.dataset.clientName,
						);
						return;
					}

					const clientId = document.querySelector(".smp-crud input#client_id");
					clientId.value = event.detail.selection.value.id;
					const selection = event.detail.selection.value.name;
					autoCompleteJS.input.value = selection;
				},
			},
		},
	};

	const autoCompleteJS = new autoComplete(config);

	document.querySelector("#client_name").addEventListener("results", function (event) {
		// "event.detail" carries the matching results values
		const modalTriggers = document.querySelectorAll(".add_item");
		modalTriggers.forEach((m) => {
			m.addEventListener("click", (e) => {
				e.preventDefault();
			});
		});
	});

	const openModal = (modalId = "", autocompleteName = "") => {
		let modal = document.querySelector(`#${modalId}`);
		modal.classList.add("smp-modal--open");
		const modalName = document.querySelector("#mclient_name");
		modalName.value = autocompleteName;
		modalName.focus();
		// document.querySelector("#main").setAttribute("inert", "");
		handleCloseModal();
	};
	const closeModal = () => {
		const existingDropdown = document.querySelector(".select-selected");
		// document.querySelector("#main").removeAttribute("inert");
		existingDropdown.remove();
		document.querySelector(".smp-modal.smp-modal--open").classList.remove("smp-modal--open");
	};
	const handleCloseModal = () => {
		const clientModalOverlay = document.querySelector(".smp-modal.smp-modal--open .smp-modal_overlay");
		clientModalOverlay.addEventListener("click", () => {
			closeModal();
		});
	};
	const closeModalBts = document.querySelectorAll(".smp-modal_close");
	closeModalBts.forEach((btn) => {
		btn.addEventListener("click", () => {
			document.querySelector(".smp-modal.smp-modal--open").classList.remove("smp-modal--open");
		});
	});

	// submit do modal
	const modalForm = document.querySelector("#modal-client form");
	modalForm.addEventListener("submit", (e) => {
		e.preventDefault(); // TODO: remover em produção
		// TODO: get Id for the newly added client and name
		updateSmpData({ client: { id: 1, name: 2 } });
		autoCompleteJS.data.store.push({ id: 1, name: "New client from form" });
		autoCompleteJS.input.value = "New client from form"; // Text show in input
		clientId.value = Math.round(Math.random() * 1000); // Id from newly created user
		closeModal();
	});

	// Get the input element
	let phoneInput = document.getElementById("mclient_phone");

	// Set the input mask
	phoneInput.addEventListener("input", function () {
		this.value = this.value.replace(/\D/g, "").replace(/^(\d{1,3})(\d{1,3})?(\d{1,4})?/, "+$1 $2 $3");
	});
});
