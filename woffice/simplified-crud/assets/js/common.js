export let smpData = {
	client: {},
	products: [{}],
	setup: {},
};

const summaryCategoriesi18n = {
	client: "Client!!!",
	products: "Produtoss!!!",
	setup: "Setupppp!!!",
};

// Used outside this function
export const updateSmpData = (newData) => {
	smpData = {
		...smpData,
		...newData,
	};
	localStorage.setItem("smpData", JSON.stringify(smpData));
};

const smpTotalSum = document.querySelector("input[name='smp-sum']");

const calculateInvoiceTotal = (invoice) => {
	let total = 0;

	// Loop through each product and calculate its total
	for (let product in invoice.products) {
		if (typeof invoice.products[product] === "object") {
			let pTotal = parseFloat(invoice.products[product].total.value);
			total += pTotal;
		}
	}

	// Subtract any setup deposit, tax, and discount
	let setupDeposit = parseFloat(invoice.setup.deposit.value);
	let setupDiscount = parseFloat(invoice.setup.discount.value);
	let setupTax = parseFloat(invoice.setup.tax.value);
	total -= setupDeposit + setupDiscount + setupTax;

	// Return the total invoice amount
	return total.toString();
};

/**
 * Gatter all data from inputs, updates smpData variable
 * and sidebar display content
 */
export const getflowInputValues = () => {
	const allInputs = document.querySelectorAll(".smp-crud input, .smp-crud textarea, .smp-crud select");
	let inputValues = [];
	let inputsClient = [];
	let inputsSetup = [];
	let inputsProducts = [];

	allInputs.forEach((input) => {
		if (input.type == "radio" && !input.checked) {
			return;
		}
		if (input.type == "checkbox") {
			input.value = input.checked;
		}
		let children = input.name.split("_");
		let i18n = input.dataset.i18n || "";

		if (children.length > 1) {
			switch (children[0]) {
				case "client": {
					inputsClient = {
						...inputsClient,
						[children[1]]: {
							value: input.value,
							i18n,
						},
					};
					break;
				}
				case "products": {
					inputsProducts = {
						...inputsProducts,
						[`product${children[1]}`]: {
							...inputsProducts[`product${children[1]}`],
							[children[2]]: {
								value: input.value,
								i18n,
							},
						},
					};
					break;
				}
				case "setup": {
					inputsSetup = {
						...inputsSetup,
						// FIX: Item 10 remove \s
						[children[1]]: {
							value: input.value.replace(/['$']/gi, ""),
							i18n,
						},
					};
					break;
				}
			}
			return;
		}

		inputValues = {
			...inputValues,
			[input.name]: {
				value: input.value,
				i18n,
			},
		};
	});

	smpData = {
		...smpData,
		client: { ...inputsClient, i18n: summaryCategoriesi18n.client },
		setup: { ...inputsSetup, i18n: summaryCategoriesi18n.setup },
		products: { ...inputsProducts, i18n: summaryCategoriesi18n.products },
		...inputValues,
	};
	smpTotalSum.value = calculateInvoiceTotal(smpData);
	smpData = {
		...smpData,
		...inputValues,
	};
	console.log(smpData);
};

const updateSummaryElement = (element, value = "") => {
	let el = document.querySelector(`span.${element}`);
	el.innerText = value;
};

// TODO: auto fill summary fields loop
export const setUpdateDataTrigger = () => {
	const allInputs = document.querySelectorAll(
		".smp-crud input:not([type='radio']):not([type='checkbox']), .smp-crud select, .smp-crud textarea",
	);
	allInputs.forEach((input) => {
		input.addEventListener("blur", function (e) {
			getflowInputValues();
		});
	});
	const allRadiosChecks = document.querySelectorAll(".smp-crud input[type=radio], .smp-crud input[type=checkbox]");
	allRadiosChecks.forEach((input) => {
		input.addEventListener("change", function () {
			getflowInputValues();
		});
	});
};
document.addEventListener("DOMContentLoaded", function () {
	setUpdateDataTrigger();
	customDropdown();
});

const menu = document.querySelector("#menu");
const summary = document.querySelector(".smp-summary");
menu.addEventListener("click", (e) => {
	e.preventDefault();
	summary.classList.toggle("open");
});

export const customDropdown = () => {
	const customPeriod = document.querySelector(".form-field__custom_period");
	const clientRepresentative = document.querySelector(".form-field--client-representative");
	var x, i, j, l, ll, selElmnt, a, b, c;
	/* Look for any elements with the class "custom-select": */
	x = document.getElementsByClassName("custom-select");
	l = x.length;
	for (i = 0; i < l; i++) {
		selElmnt = x[i].getElementsByTagName("select")[0];
		ll = selElmnt.length;
		/* For each element, create a new DIV that will act as the selected item: */
		a = document.createElement("DIV");
		a.setAttribute("class", "select-selected");
		a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
		x[i].appendChild(a);
		/* For each element, create a new DIV that will contain the option list: */
		b = document.createElement("DIV");
		b.setAttribute("class", "select-items select-hide");
		for (j = 1; j < ll; j++) {
			/* For each option in the original select element,
    create a new DIV that will act as an option item: */
			c = document.createElement("DIV");
			c.innerHTML = selElmnt.options[j].innerHTML;
			c.addEventListener("click", function (e) {
				/* When an item is clicked, update the original select box,
        and the selected item: */
				var y, i, k, s, h, sl, yl;
				s = this.parentNode.parentNode.getElementsByTagName("select")[0];
				sl = s.length;
				h = this.parentNode.previousSibling;
				for (i = 0; i < sl; i++) {
					if (s.options[i].innerHTML == this.innerHTML) {
						s.selectedIndex = i;

						// Toggle representative
						if (s.value === "1") {
							clientRepresentative.style.display = "block";
							clientRepresentative.querySelector("input").setAttribute("required", true);
						} else {
							clientRepresentative.style.display = "none";
							clientRepresentative.querySelector("input").removeAttribute("required");
						}

						// Toggle custom period
						if (s.value === "custom") {
							customPeriod.style.display = "grid";
						} else {
							// FIX: Item 1
							if (s.name === "mproduct_custom-recurring-period") return;
							customPeriod.style.display = "none";
						}

						h.innerHTML = this.innerHTML;
						y = this.parentNode.getElementsByClassName("same-as-selected");
						yl = y.length;
						for (k = 0; k < yl; k++) {
							y[k].removeAttribute("class");
						}
						this.setAttribute("class", "same-as-selected");
						break;
					}
				}
				h.click();
			});
			b.appendChild(c);
		}
		x[i].appendChild(b);
		a.addEventListener("click", function (e) {
			/* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
			e.stopPropagation();
			closeAllSelect(this);
			this.nextSibling.classList.toggle("select-hide");
			this.classList.toggle("select-arrow-active");
		});
	}

	function closeAllSelect(elmnt) {
		/* A function that will close all select boxes in the document,
  except the current select box: */
		var x,
			y,
			i,
			xl,
			yl,
			arrNo = [];
		x = document.getElementsByClassName("select-items");
		y = document.getElementsByClassName("select-selected");
		xl = x.length;
		yl = y.length;
		for (i = 0; i < yl; i++) {
			if (elmnt == y[i]) {
				arrNo.push(i);
			} else {
				y[i].classList.remove("select-arrow-active");
			}
		}
		for (i = 0; i < xl; i++) {
			if (arrNo.indexOf(i)) {
				x[i].classList.add("select-hide");
			}
		}
	}

	/* If the user clicks anywhere outside the select box,
then close all select boxes: */
	document.addEventListener("click", closeAllSelect);
};
