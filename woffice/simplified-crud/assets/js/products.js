import { printJSONAsHTML, printSummary } from "./summary.js";
import { customDropdown, updateSmpData, setUpdateDataTrigger, smpData, getflowInputValues } from "./common.js";

export const formatCurrency = (amount) => {
	return new Intl.NumberFormat("en-US", { maximumSignificantDigits: 4 }).format(amount);
};

let autoCompleteJS = new Array();

document.addEventListener("DOMContentLoaded", () => {
	const newProductBtn = document.querySelector(".smp-products_new");
	const productItem = document.querySelector(".smp-products_list > .smp-product");
	const recommendatiosWrapper = document.querySelector(".smp-products_recommendations");
	let productItems;
	let recommendations;

	const getProductItems = () => {
		productItems = document.querySelectorAll(".smp-products_list .smp-product input:not(type[hidden])");
	};

	const getNewRecommendations = (productId) => {
		// TODO: get recommendation list via ajax
		recommendations = [
			`Product Recommended ${Math.round(new Date() / 1000)}`,
			`Product Recommended ${Math.round(new Date() / 1000)}`,
			`Product Recommended ${Math.round(new Date() / 1000)}`,
			`Product Recommended ${Math.round(new Date() / 1000)}`,
			`Product Recommended ${Math.round(new Date() / 1000)}`,
		];
		// TODO: clear and append new list items to UL
		listRecommendationItems();
	};

	const listRecommendationItems = () => {
		// TODO: Refactor for real data;
		const recommendationUl = document.querySelector(".smp-products_recommendations ul");
		recommendationUl.innerHTML = ""; // clean UL
		let li = document.createElement("li");
		li.innerHTML = "Recomendations:";
		recommendationUl.append(li);
		recommendations.forEach((r) => {
			li = document.createElement("li");
			let a = document.createElement("a");
			a.href = "#";
			a.dataset.productId = Math.round(new Date() / 1000);
			a.innerHTML = r;
			li.append(a);
			recommendationUl.append(li);
		});
		setRecommendationsTriggers();
	};

	const setRecommendationsTriggers = () => {
		const recommendations = document.querySelectorAll(".smp-products_recommendations li a");
		recommendations.forEach((r) => {
			r.addEventListener("click", (e) => {
				setupNewProduct();
				let data = {
					id: e.target.dataset.productId,
					name: e.target.innerHTML,
				};
				selectRecommendation(data);
				getNewRecommendations();
			});
		});
	};

	const selectRecommendation = (productData) => {
		let productRefId = parseInt(document.querySelectorAll(".smp-products_list .smp-product").length);
		const productTotalInput = document.querySelector(`input[name="products_${productRefId}_total"]`);
		const productTotalText = document.querySelector(`#smp-product-${productRefId} .form-field--product-total span`);
		const productPriceInput = document.querySelector(`input[name="products_${productRefId}_price"]`);
		const productPriceText = document.querySelector(
			`#smp-product-${productRefId} .form-field--product-price input`,
		);
		const productName = document.querySelector(`input[name="products_${productRefId}_name"]`);
		const productQty = document.querySelector(`input[name="products_${productRefId}_qty"]`);
		const productDiscount = document.querySelector(`input[name="products_${productRefId}_discount"]`);
		const productTax = document.querySelector(`input[name="products_${productRefId}_tax"]`);

		// TODO: add correct data from ajax
		[
			productName.value,
			productTotalInput.value,
			productPriceInput.value,
			productQty.value,
			productDiscount.value,
			productTax.value,
		] = [productData.name, 10.11, 10.11, 1, 0, 0];
		productPriceText.innerHTML = "$" + formatCurrency(productPriceInput.value);
		productTotalText.innerHTML = "$" + formatCurrency(productTotalInput.value);
	};

	const setDeleteBtns = () => {
		const deleteProductBtns = document.querySelectorAll(".smp-product_close");
		deleteProductBtns.forEach((btn) => {
			btn.addEventListener("click", () => {
				btn.parentNode.remove();
				getflowInputValues();
				printJSONAsHTML(smpData);
			});
		});
	};

	const setupNewProduct = () => {
		// Código do protótipo
		let productClone = productItem.cloneNode(true);
		let nextId = document.querySelectorAll(".smp-products_list .smp-product").length + 1;
		productClone.id = productClone.id.replace("-1", `-${nextId}`);
		productClone.querySelectorAll("input").forEach((input) => {
			input.name = input.name.replace("_1_", `_${nextId}_`);
			// FIX: Item 6
			if (input.type == "number") {
				input.value = "0";
			} else {
				input.value = "";
			}
		});
		productClone.querySelector(".form-field--product-total span").innerHTML = "";
		document.querySelector(".smp-products_list").append(productClone);

		// Código de protótipo

		setDeleteBtns();
		getProductItems();
		addCalculateTrigger();
		setUpProductAutocomplete(nextId);
		setUpdateDataTrigger();
		printSummary();
	};

	newProductBtn.addEventListener("click", () => {
		setupNewProduct();
	});

	const calculateItemTotal = (productRefId) => {
		const productTotalInput = document.querySelector(`input[name="products_${productRefId}_total"]`);
		const productTotalText = document.querySelector(`#smp-product-${productRefId} .form-field--product-total span`);
		const productPrice = parseFloat(document.querySelector(`input[name="products_${productRefId}_price"]`).value);
		const productQty = parseInt(document.querySelector(`input[name="products_${productRefId}_qty"]`).value);
		const productDiscount = parseFloat(
			document.querySelector(`input[name="products_${productRefId}_discount"]`).value,
		);
		const productTax = parseFloat(document.querySelector(`input[name="products_${productRefId}_tax"]`).value);
		const calcTotal = formatCurrency(productQty * productPrice - productDiscount + productTax);
		productTotalInput.value = calcTotal;
		productTotalText.innerHTML = `$ ${calcTotal}`;
	};

	const addCalculateTrigger = () => {
		productItems.forEach((pi) => {
			pi.addEventListener("input", () => {
				calculateItemTotal(pi.name.split("_")[1]);
			});
		});
	};

	// Auto complete
	const setUpProductAutocomplete = (productRefId = 1) => {
		let productId = document.querySelector(`input[name=products_${productRefId}_id`);
		let products = document.querySelectorAll(".form-field--product-name");
		let config = {
			// API Advanced Configuration Object
			selector: `input[name=products_${productRefId}_name`,
			placeHolder: "Find a product...",
			data: {
				src: async (query) => {
					try {
						// Fetch Data from external Source
						const source = await fetch(`assets/js/fakedata/products.json?query=${query}`);
						// Data should be an array of `Objects` or `Strings`
						const data = await source.json();
						// console.log(data);

						return data;
					} catch (error) {
						return error;
					}
				},
				// Data source 'Object' key to be searched
				keys: ["id", "name"],
			},
			resultsList: {
				element: (list, data) => {
					if (!data.results.length) {
						// Create "No Results" message element
						const message = document.createElement("div");
						// Add class to the created element
						message.setAttribute("class", "no_result");
						// Add message text content
						message.innerHTML = `<span>No product named "${data.query}"</span>`;
						// Append message element to the results list
						list.prepend(message);
					}
					const message = document.createElement("li");
					message.setAttribute("class", "add_item");
					message.setAttribute("data-trigger", "modal-product");
					message.setAttribute("data-product-name", data.query);
					message.innerHTML = `<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M12 0C10.8954 0 10 0.89543 10 2V10H2C0.895431 10 0 10.8954 0 12C0 13.1046 0.89543 14 2 14H10V22C10 23.1046 10.8954 24 12 24C13.1046 24 14 23.1046 14 22V14H22C23.1046 14 24 13.1046 24 12C24 10.8954 23.1046 10 22 10H14V2C14 0.895431 13.1046 0 12 0Z" fill="currentColor"/>
					</svg>
					Add <mark>${data.query}</mark> as a new product`;
					list.append(message);
				},
				noResults: true,
			},
			resultItem: {
				highlight: true,
			},
			events: {
				input: {
					selection: (event) => {
						// TODO: change ID
						// let nextListId = autoCompleteJS.data.store.length + 1;

						if (event.detail.event.target.classList.contains("add_item")) {
							event.preventDefault();
							// Capture modal trigger
							openModal(
								event.detail.event.target.dataset.trigger,
								event.detail.event.target.dataset.productName,
								productRefId,
							);
							return;
						}

						const productTotalInput = document.querySelector(
							`input[name="products_${productRefId}_total"]`,
						);
						const productTotalText = document.querySelector(
							`#smp-product-${productRefId} .form-field--product-total span`,
						);
						const productPrice = document.querySelector(`input[name="products_${productRefId}_price"]`);
						const productQty = document.querySelector(`input[name="products_${productRefId}_qty"]`);
						const productDiscount = document.querySelector(
							`input[name="products_${productRefId}_discount"]`,
						);
						const productTax = document.querySelector(`input[name="products_${productRefId}_tax"]`);

						// TODO: add correct data from ajax
						// productTotalInput.value = event.detail.selection.value.price;
						// productPriceInput.value = event.detail.selection.value.price;
						// productQty.value = event.detail.selection.value.qty;
						// productDiscount.value = event.detail.selection.value.discount;
						// productTax.value = event.detail.selection.value.tax;
						// productTotalText.innerHTML = "$ " + formatCurrency(event.detail.selection.value.price);
						// productPriceText.innerHTML = "$ " + formatCurrency(event.detail.selection.value.price);
						productId.value = event.detail.selection.value.id;
						const selection = event.detail.selection.value.name;

						autoCompleteJS[productRefId - 1].input.value = selection;
						getNewRecommendations();
						recommendatiosWrapper.style.display = "block";
					},
				},
			},
		};
		// FIX: Item 5
		autoCompleteJS.push(new autoComplete(config));

		const openModal = (modalId = "", autocompleteName = "", productRefId = 1) => {
			let modal = document.querySelector(`#${modalId}`);
			modal.classList.add("smp-modal--open");
			modal.querySelector("form").setAttribute("data-productRefId", productRefId);
			const modalName = document.querySelector("#mproduct_name");
			modalName.value = autocompleteName;
			modalName.focus();
			// document.querySelector("#main").setAttribute("inert", "");
			handleCloseModal();
		};

		const handleCloseModal = () => {
			const clientModalOverlay = document.querySelector(".smp-modal.smp-modal--open .smp-modal_overlay");
			clientModalOverlay.addEventListener("click", () => {
				closeModal();
			});
			const closeModalBts = document.querySelectorAll(".smp-modal_close");
			closeModalBts.forEach((btn) => {
				btn.addEventListener("click", () => {
					closeModal();
				});
			});
		};

		// Show image
		document.getElementById("mproduct_img").onchange = function () {
			var src = URL.createObjectURL(this.files[0]);
			let img = document.getElementById("mproduct_img-preview");
			let upload = document.querySelector(".form-field__img .file-upload");
			img.src = src;
			img.style.display = "block";
			upload.style.display = "none";
		};

		// Calculate total when input
		const priceInput = document.getElementById("mproduct_price");
		const qtyInput = document.getElementById("mproduct_qty");
		const discountInput = document.getElementById("mproduct_discount");
		const taxInput = document.getElementById("mproduct_tax");
		const totalInput = document.getElementById("mproduct_total");

		function updateTotal() {
			const price = parseFloat(priceInput.value || 0);
			const qty = parseInt(qtyInput.value || 0);
			const discount = parseFloat(discountInput.value || 0);
			const tax = parseFloat(taxInput.value || 0);

			const total = price * qty * (1 - discount / 100) * (1 + tax / 100);
			totalInput.value = total.toFixed(2);
		}

		priceInput.addEventListener("blur", updateTotal);
		qtyInput.addEventListener("blur", updateTotal);
		discountInput.addEventListener("blur", updateTotal);
		taxInput.addEventListener("blur", updateTotal);

		// Toggle recurring
		const reccuringToggle = document.querySelector("#mproduct_recurring-toggle");
		const recurringPeriod = document.querySelector(".form-field--product-period");

		reccuringToggle.addEventListener("change", (e) => {
			if (reccuringToggle.checked) {
				recurringPeriod.style.display = "block";
				return;
			}
			recurringPeriod.style.display = "none";
		});
	};
	const closeModal = () => {
		document.querySelector(".smp-modal.smp-modal--open").classList.remove("smp-modal--open");
		// document.querySelector("#main").removeAttribute("inert");
	};
	// submit do modal
	const modalForm = document.querySelector("#modal-product form");
	modalForm.addEventListener("submit", (e) => {
		e.preventDefault(); // TODO: remover em produção
		let prodId = parseInt(e.target.dataset.productrefid);
		let productId = document.querySelector(`input[name=products_${prodId}_id`);
		// TODO: get Id for the newly added product and name
		updateSmpData({ client: { id: 1, name: 2 } });
		autoCompleteJS[prodId - 1].data.store.push({ id: 1, name: "New product from form" });
		autoCompleteJS[prodId - 1].input.value = "New product from form"; // Text show in input
		productId.value = Math.round(Math.random() * 1000); // Id from newly created user
		console.log("SUBMIT");
		closeModal();
	});

	getProductItems();
	addCalculateTrigger();
	setUpProductAutocomplete();
});
