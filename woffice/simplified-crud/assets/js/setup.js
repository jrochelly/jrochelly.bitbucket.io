document.addEventListener("DOMContentLoaded", () => {
	const includeFeeToggle = document.querySelector("#setup_include-fee");
	const processingFeesBox = document.querySelector(".processing-fees");
	const smpType = document.querySelectorAll("input[name='smp-type']");
	const setupDeposit = document.querySelector(".setup_deposit");
	const submitFormBtn = document.querySelector(".submit-form");

	if (includeFeeToggle.checked) processingFeesBox.classList.add("processing-fees--active");

	includeFeeToggle.addEventListener("change", () => {
		processingFeesBox.classList.toggle("processing-fees--active");
	});

	// When the Select type is changed
	smpType.forEach((t) => {
		t.addEventListener("click", () => {
			if (t.value == 0) {
				setupDeposit.style.display = "block";
				submitFormBtn.innerHTML = "Save Estimate";
			} else {
				setupDeposit.style.display = "none";
				submitFormBtn.innerHTML = "Save Invoice";
			}
		});
	});

	const discountTypeRadio = document.querySelectorAll('input[name="discount-type"]');
	const discountInput = document.querySelector('input[name="setup_discount"]');

	function handleDiscountInput() {
		const parsedValue = parseFloat(discountInput.value.replace(/\D/g, ""));
		if (isNaN(parsedValue)) {
			discountInput.value = "";
		} else {
			discountInput.value = parsedValue + "%";
		}
	}

	discountTypeRadio.forEach((radio) => {
		radio.addEventListener("change", () => {
			const previousValue = discountInput.value;
			console.log(previousValue);

			if (radio.value === "percentage") {
				discountInput.type = "text";
				discountInput.placeholder = "Enter discount percentage";

				handleDiscountInput();
				discountInput.addEventListener("input", handleDiscountInput);
			} else {
				discountInput.type = "number";
				discountInput.value = parseFloat(previousValue) || 0;
				discountInput.placeholder = "";
				discountInput.removeEventListener("input", handleDiscountInput);
			}
			discountInput.focus();
		});
	});
});
