import { smpData } from "./common.js";
import { formatCurrency } from "./products.js";

function convertDate(string) {
	var date = new Date(string);
	return new Intl.DateTimeFormat("en-US").format(date);
	// return date.toLocaleString("en-US");
}

export const printSummary = () => {
	let summaryWrapper = document.querySelector(".smp-summary .summary__wrapper");
	const allInputs = document.querySelectorAll(
		".smp-crud input:not([type='radio']):not([type='checkbox']), .smp-crud select, .smp-crud textarea",
	);

	allInputs.forEach((input) => {
		input.addEventListener("blur", function (e) {
			printJSONAsHTML(smpData);
		});
	});
	const allRadiosChecks = document.querySelectorAll(".smp-crud input[type=radio], .smp-crud input[type=checkbox]");
	allRadiosChecks.forEach((input) => {
		input.addEventListener("change", function () {
			printJSONAsHTML(smpData);
		});
	});
};

function toTitleCase(str) {
	return str.replace(/\w\S*/g, function (txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

export const printJSONAsHTML = (json) => {
	let summaryWrapper = document.querySelector(".smp-summary .summary__wrapper");
	summaryWrapper.innerHTML = "";
	const keysToIgnore = ["id", "discount-type", "i18n"];
	const keysToFormat = ["tax", "price", "total", "smp-sum", "discount"];

	for (let [key, value] of Object.entries(json)) {
		if (!keysToIgnore.includes(key)) {
			const section = document.createElement("section");
			section.classList.add(`summary__${key}`);
			const heading = document.createElement("h3");
			heading.textContent = toTitleCase(value.i18n);

			if (key.includes("smp-sum")) {
				const heading = document.createElement("h3");
				heading.textContent = value.i18n;
				section.appendChild(heading);
				const subSection = document.createElement("div");
				const subHeading = document.createElement("h4");
				subHeading.textContent = "$" + value.value;
				subSection.appendChild(subHeading);
				section.appendChild(subSection);
			} else {
				section.appendChild(heading);
			}

			const table = document.createElement("table");
			let hasCreatedTableHead = false;
			for (let [subkey, subvalue] of Object.entries(value)) {
				if (subvalue && !keysToIgnore.includes(subkey) && key !== "smp-sum") {
					if (subkey.includes("product")) {
						const tbodyRow = document.createElement("tr");

						const theadRow = document.createElement("tr");
						// FIX: Item 4
						if (typeof subvalue === "object") {
							if (hasCreatedTableHead == false) {
								for (const [subSubkey, subSubvalue] of Object.entries(subvalue)) {
									// FIX: Item 7
									if (!keysToIgnore.includes(subSubkey)) {
										const thd = document.createElement("th");
										thd.textContent = toTitleCase(subSubvalue.i18n);
										theadRow.appendChild(thd);
									}
								}
								hasCreatedTableHead = true;
							}
							for (const [subSubkey, subSubvalue] of Object.entries(subvalue)) {
								// FIX: Item 7
								if (!keysToIgnore.includes(subSubkey)) {
									const tbd = document.createElement("td");

									if (keysToFormat.includes(subSubkey)) {
										// FIX: Item 9
										tbd.textContent = "$" + formatCurrency(subSubvalue.value.replace(/[,]/g, ""));
									} else {
										tbd.textContent = subSubvalue.value;
									}
									tbodyRow.appendChild(tbd);
								}
							}
						}
						table.appendChild(theadRow);
						table.appendChild(tbodyRow);

						section.appendChild(table);
					} else {
						const subSection = document.createElement("div");
						const subHeading = document.createElement("h4");
						subHeading.textContent = toTitleCase(subvalue.i18n);
						subSection.appendChild(subHeading);

						if (typeof subvalue === "object") {
							for (const [subSubkey, subSubvalue] of Object.entries(subvalue)) {
								if (subSubvalue && subSubkey !== "name" && !keysToIgnore.includes(subSubkey)) {
									const subSubSection = document.createElement("div");
									// const subSubHeading = document.createElement("h5");

									const subSubParagraph = document.createElement("p");
									if (subkey === "start-date") {
										subSubParagraph.textContent = convertDate(subSubvalue);
									} else if (keysToFormat.includes(subkey)) {
										subSubParagraph.textContent = "$" + formatCurrency(subSubvalue);
									} else if (subkey == "include-fee") {
										subSubParagraph.textContent = subSubvalue == "true" ? "Yes" : "No";
									} else if (subkey === "fee-type") {
										if (json.setup["include-fee"].value == "false") {
											subSubParagraph.textContent = "";
										} else {
											switch (subSubvalue) {
												case "1":
													subSubParagraph.textContent = "Separeted item";
													break;
												case "2":
													subSubParagraph.textContent = "Embeded price";
													break;
												case "3":
													subSubParagraph.textContent = "After transaction";
													break;
												default:
													subSubParagraph.textContent = "Not applied";
											}
										}
									} else {
										subSubParagraph.textContent = subSubvalue;
									}

									subSubSection.appendChild(subSubParagraph);
									subSection.appendChild(subSubSection);
								}
							}
						}
						section.appendChild(subSection);
					}
				}
			}

			summaryWrapper.appendChild(section);
		}
	}
};
document.addEventListener("DOMContentLoaded", function () {
	printSummary();
});
