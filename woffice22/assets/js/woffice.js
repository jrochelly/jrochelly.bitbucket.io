document.addEventListener("DOMContentLoaded", () => {
	const playButton = document.querySelector("button.play");
	const mobileTrigger = document.querySelector(".mobile-trigger");
	const menu = document.querySelector(".menu");
	const faqs = document.querySelectorAll(".faq-question a");

	mobileTrigger.addEventListener("click", () => {
		menu.classList.toggle("active");
	});

	document.addEventListener("click", (e) => {
		if (
			e.target !== mobileTrigger &&
			e.target !== menu &&
			!menu.contains(e.target) &&
			!mobileTrigger.contains(e.target)
		) {
			menu.classList.remove("active");
		}
	});

	playButton.addEventListener("click", () => {
		const video = document.querySelector("video");
		video.play();
		playButton.remove();
		video.setAttribute("controls", "controls");
	});

	// FAQs accordion
	faqs.forEach((faq) => {
		faq.addEventListener("click", (e) => {
			e.preventDefault();
			let isExpanded = faq.getAttribute("aria-expanded");
			faq.setAttribute("aria-expanded", !isExpanded);
			faq.parentNode.classList.toggle("open");
			faqs.forEach((faqToClose) => {
				if (faqToClose === faq) return;
				faqToClose.setAttribute("aria-expanded", "false");
				faqToClose.parentNode.classList.remove("open");
			});
			setTimeout(() => {
				faq.parentNode.scrollIntoView({
					block: "nearest",
					behavior: "smooth",
				});
			}, 100);
		});
	});

	var typed = new Typed(".hero-typed span", {
		strings: ["NO WASTE,", "NO WORRIES,"],
		typeSpeed: 60,
	});

	particlesJS("particles-js", {
		particles: {
			number: { value: 80, density: { enable: true, value_area: 800 } },
			color: { value: "#ffffff" },
			shape: {
				type: "circle",
				stroke: { width: 0, color: "#000000" },
				polygon: { nb_sides: 5 },
				image: { src: "img/github.svg", width: 100, height: 100 },
			},
			opacity: {
				value: 0.5,
				random: false,
				anim: {
					enable: false,
					speed: 1,
					opacity_min: 0.1,
					sync: false,
				},
			},
			size: {
				value: 3,
				random: true,
				anim: { enable: false, speed: 40, size_min: 0.1, sync: false },
			},
			line_linked: {
				enable: true,
				distance: 150,
				color: "#ffffff",
				opacity: 0.4,
				width: 1,
			},
			move: {
				enable: true,
				speed: 6,
				direction: "none",
				random: false,
				straight: false,
				out_mode: "out",
				bounce: false,
				attract: { enable: false, rotateX: 600, rotateY: 1200 },
			},
		},
		interactivity: {
			detect_on: "canvas",
			events: {
				onhover: { enable: false, mode: "grab" },
				onclick: { enable: true, mode: "push" },
				resize: true,
			},
			modes: {
				grab: {
					distance: 155.84415584415586,
					line_linked: { opacity: 1 },
				},
				bubble: {
					distance: 400,
					size: 40,
					duration: 2,
					opacity: 8,
					speed: 3,
				},
				repulse: { distance: 200, duration: 0.4 },
				push: { particles_nb: 4 },
				remove: { particles_nb: 2 },
			},
		},
		retina_detect: true,
	});
});
